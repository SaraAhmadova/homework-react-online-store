import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProductCard from './ProductCard';

export class ProductList extends Component {
    static propTypes = {
        products: PropTypes.array.isRequired,
        onAddToCart: PropTypes.func.isRequired,
        onToggleFavorite: PropTypes.func.isRequired,
        favorites: PropTypes.array.isRequired,
      };
  render() {
    const { products, onAddToCart, onToggleFavorite, favorites } = this.props;

    return (
      <div className="product-list">
        {products.map((product) => (
          <ProductCard
            key={product.sku}
            product={product}
            onAddToCart={onAddToCart}
            onToggleFavorite={onToggleFavorite}
            isFavorite={favorites.includes(favorites.find(fav=> fav.sku === product.sku))}
          />
        ))}
      </div>
    );
  }
}

export default ProductList