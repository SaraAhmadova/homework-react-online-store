import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Cart extends Component {
  static propTypes = {
    cartCount: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired
  };

  render() {
    const { cartCount, onClick } = this.props;

    return (
      <div className="cart" onClick={onClick}>
        <span role="img" aria-label="cart">
          🛒
        </span>
        {cartCount > 0 && <span className="cart-count">{cartCount}</span>}
      </div>
    );
  }
}

export default Cart;