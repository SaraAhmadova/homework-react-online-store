import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './favoritesModal.css'

class FavoritesModal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
  };

  render() {
    const { isOpen, onClose, favorites } = this.props;

    return (
      isOpen && (
        <div className="favorites-modal" onClick={onClose}>
          <div className="favorites-modal-content" onClick={(e) => e.stopPropagation()}>
            <span className="favorites-modal-close" onClick={onClose}>
              &times;
            </span>
            <h2>Favorites</h2>
            <ul>
              {favorites.map((product, index) => {
                console.log(favorites);
                return (
                    <li key={index}>
                      {product.name} - ${product.price}
                    </li>
                  )
              })}
            </ul>
          </div>
        </div>
      )
    );
  }
}

export default FavoritesModal;
