import PropTypes from "prop-types";
import React, { Component } from "react";

export default class CartModal extends Component {
  static propTypes = {
    cart: PropTypes.array.isRequired,
    closeCartModal: PropTypes.func.isRequired,
  };

  render() {
    const { cart, closeCartModal } = this.props;
    return (
      <div className="cart-modal" onClick={closeCartModal}>
        <div
          className="cart-modal-content"
          onClick={(e) => e.stopPropagation()}
        >
          <span className="cart-modal-close" onClick={closeCartModal}>
            &times;
          </span>
          <h2>Shopping Cart</h2>
          <ul>
            {cart.map((product, index) => (
              <li key={index}>
                {product.name} - ${product.price}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
