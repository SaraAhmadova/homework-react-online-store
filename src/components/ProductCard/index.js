import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ProductCard extends Component {
  static propTypes = {
    product: PropTypes.shape({
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      imagePath: PropTypes.string.isRequired,
      sku: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
    }).isRequired,
    onAddToCart: PropTypes.func.isRequired,
    onToggleFavorite: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool.isRequired,
  };

  render() {
    const { product, onAddToCart, onToggleFavorite, isFavorite } = this.props;

    return (
      <div className="product-card">
        <img src={product.imagePath} alt={product.name} className="product-image" />
        <h3 className="product-name">{product.name}</h3>
        <p className="product-price">${product.price}</p>
        <p className="product-color">Color: {product.color}</p>
        <button onClick={() => onAddToCart(product)} className='card-btn'><span role="img" aria-label="cart">
              🛒
            </span></button>
        <button onClick={() => onToggleFavorite(product)} className='card-btn'>
          {isFavorite ? <span className="favorite-star">&#9733;</span>:  <span className="unfavorite-star">&#9733;</span>}
        </button>
      </div>
    );
  }
}

export default ProductCard;
