import { Component } from "react";
import "./header.css";
export class Header extends Component {
  render() {
    return (
      <ul className={"list-container"}>
        <li className={"list-item"}>
          <button onClick={this.props.onFavoritesClick}>
            <span role="img" aria-label="favorites">
              ⭐
            </span>
            <span>Favorites: {this.props.favoritesCount}</span>
          </button>
        </li>
        <li className={"list-item"}>
          <button  onClick={this.props.onCartClick}> 
            <span role="img" aria-label="cart">
              🛒
            </span>
            <span>Cart: {this.props.cartCount}</span>
          </button>
        </li>
      </ul>
    );
  }
}
