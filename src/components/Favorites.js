import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Favorites extends Component {
  static propTypes = {
    favoritesCount: PropTypes.number.isRequired,
  };

  render() {
    const { favoritesCount } = this.props;

    return (
      <div className="favorites">
        <span role="img" aria-label="favorites">
          ⭐
        </span>
        {favoritesCount > 0 && <span className="favorites-count">{favoritesCount}</span>}
      </div>
    );
  }
}

export default Favorites;
