import React, { Component } from "react";
import "./App.css";
import ProductList from "./components/ProductList";
import CartModal from "./components/CartModal";
import { Header } from "./components/header";
import FavoritesModal from "./components/favoritesModal";

class App extends Component {
  state = {
    products: [],
    cart: JSON.parse(localStorage.getItem("cart")) || [],
    favorites: JSON.parse(localStorage.getItem("favorites")) || [],
    isCartModalOpen: false,
    isFavoritesModalOpen: false,
  };

  componentDidMount() {
    fetch("/products.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ products: data });
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }
  addToCart = (product) => {
    this.setState(
      (prevState) => ({ cart: [...prevState.cart, product] }),
      () => {
        localStorage.setItem("cart", JSON.stringify(this.state.cart));
      }
    );
  };
  toggleCartModal = () => {
    this.setState((prevState) => ({
      isCartModalOpen: !prevState.isCartModalOpen,
    }));
  };

  closeCartModal = () => {
    this.setState({ isCartModalOpen: false });
  };
  toggleFavModal = () => {
    this.setState((prevState) => ({
      isFavoritesModalOpen: !prevState.isFavoritesModalOpen,
    }));
  };

  closeFavModal = () => {
    this.setState({ isFavoritesModalOpen: false });
  };
  toggleFavorite = (product) => {
    this.setState((prevState) => ({
        favorites: prevState.favorites.includes(this.state.products.find(prodct=> prodct.sku === product.sku))
          ? prevState.favorites.filter((item) => item.sku !== product.sku)
          : [...prevState.favorites, product],
      })
    );
  };
  render() {
    const { products, cart, favorites, isCartModalOpen, isFavoritesModalOpen } = this.state;

    return (
      <div>
        <h1>Online Store</h1>
        <Header
          favoritesCount={favorites.length}
          cartCount={cart.length}
          onCartClick={this.toggleCartModal}
          onFavoritesClick={this.toggleFavModal}
        />
        {products && (
          <ProductList
            products={products}
            onAddToCart={this.addToCart}
            onToggleFavorite={this.toggleFavorite}
            favorites={favorites}
          />
        )}
        {isCartModalOpen && (
          <CartModal cart={cart} closeCartModal={this.closeCartModal} />
        )}
        <FavoritesModal
          onClose={this.closeFavModal}
          isOpen={isFavoritesModalOpen}
          favorites={favorites}
        />
      </div>
    );
  }
}

export default App;
